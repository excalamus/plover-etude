![Songbird](plover_etude/resources/bird_96.png)

# Plover Etude
A Plover plugin to improve steno writing.

![Plover Etude](plover_etude/resources/screenshot.png)
